
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Document } from 'react-pdf/dist/entry.webpack';
import DrawablePage from './components/DrawablePage/DrawablePage';

 class SceneWithDrawables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numPages: null,
      pageNumber: 1
    };
  }
  onDocumentLoadSuccess = (pdf) => {
    this.setState({ numPages: pdf.numPages });
  };
  render() {
    const { pageNumber, numPages, page_width, page_height, newDrawableType } = this.state;
    const {onChangeFigureType} = this.props;
    debugger;
    return (<div>
      <button onClick={e => {
        onChangeFigureType("ArrowDrawable");
      }}>
        Draw Arrows
        </button>
      <button onClick={e => {
        onChangeFigureType("CircleDrawable");
      }}>
        Draw Circles
        </button>
      <button onClick={e => {
        onChangeFigureType("FreePathDrawable");
      }}>
        Draw FreeHand!
        </button>

      <button onClick={e => {
        onChangeFigureType("MarkerDrawable");
      }}>
        Marker!
        </button>
      <Document file="../sample.pdf" onLoadSuccess={this.onDocumentLoadSuccess}>
        <DrawablePage pageNumber={pageNumber} onLoadSuccess={this.onPageLoadSuccess} />
      </Document>
    </div>);
  }
}


export default connect(
    (state, ownProps) => {
        return {
            //drawable: state.drawable,
            ownProps
          };
    },
      dispatch => {
          return {
            
            onChangeFigureType: (figure_type) => {
              const payload = figure_type;
              dispatch({ type: 'CHANGE_FIGURE_TYPE', payload });
            },
            
          };
      }
)(SceneWithDrawables)