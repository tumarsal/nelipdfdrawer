import React from 'react';
import logo from './logo.svg';
import Konva, { Stage, Layer, Arrow, Circle, Line } from "react-konva";
import { Page } from 'react-pdf/dist/entry.webpack';
import './App.css';
import SceneWithDrawables from './SceneWithDrawables';

//DRAWING END 


function App() {
  return (
    <div className="App">
      <SceneWithDrawables></SceneWithDrawables>
      
    </div>
  );
}

export default App;
