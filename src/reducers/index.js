
export default function reducer(state = {value:'default',
            drawables: [],
            newDrawableType: "MarkerDrawable",
}, action) {
  switch (action.type){
    case 'ADD_FIGURE':
      const newState =  {
        drawables: [...state.drawables,action.payload],
        newDrawableType : state.newDrawableType
      };
      return newState;
    break;
    case 'CHANGE_FIGURE_TYPE':
      return {
        drawables:state.drawables,
        newDrawableType:action.payload
      }
      break;
  }
  return state;
}
  