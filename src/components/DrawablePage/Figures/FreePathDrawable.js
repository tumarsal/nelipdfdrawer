import React from 'react';
import { Line } from "react-konva";
import { Drawable } from './Drawable';
export default class FreePathDrawable extends Drawable {
    constructor(startx, starty) {
        super(startx, starty);
        this.points = [startx, starty];
    }
    registerMovement(x, y) {
        this.points = [...this.points, x, y];
    }
    render() {
        return <Line points={this.points} fill="black" stroke="black" />;
    }
}
