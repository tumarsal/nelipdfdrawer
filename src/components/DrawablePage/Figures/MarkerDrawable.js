import React from 'react';
import Konva from "react-konva";
import { Drawable } from './Drawable';
export class MarkerDrawable extends Drawable {
    constructor(startx, starty) {
        super(startx, starty);
        this.x = startx;
        this.y = starty;
    }
    registerMovement(x, y) {
        this.x = x;
        this.y = y;
    }
    render() {
        var imageObj = new Image();
        imageObj.src = "../pizza.svg";
        return (<Konva.Image x={this.x} y={this.y} image={imageObj}></Konva.Image>);
    }
}
