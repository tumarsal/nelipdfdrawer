
//DRAWING 

import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Stage, Layer } from "react-konva";
import {  Page } from 'react-pdf/dist/entry.webpack';
import FreePathDrawable from './Figures/FreePathDrawable';
import { ArrowDrawable } from './Figures/ArrowDrawable';
import { MarkerDrawable } from './Figures/MarkerDrawable';
import { CircleDrawable } from './Figures/CircleDrawable';
import './DrawablePage.css';
class DrawablePage extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            newDrawable: [],
        };
        

    }

    getNewDrawableBasedOnType = (x, y, type) => {
        const drawableClasses = {
            FreePathDrawable,
            ArrowDrawable,
            CircleDrawable,
            MarkerDrawable
        };
        return new drawableClasses[type](x, y);
    };

    handleMouseDown = e => {
        const { newDrawable } = this.state;
        if (newDrawable.length === 0) {
            const { x, y } = e.target.getStage().getPointerPosition();
            const newDrawable = this.getNewDrawableBasedOnType(
                x,
                y,
                this.props.newDrawableType
            );
            this.setState({
                newDrawable: [newDrawable]
            });
        }
    };

    handleMouseUp = e => {
        const { newDrawable, drawables } = this.state;
        const {addFigure} = this.props;
        if (newDrawable.length === 1) {
            const { x, y } = e.target.getStage().getPointerPosition();
            const drawableToAdd = newDrawable[0];
            drawableToAdd.registerMovement(x, y);
            addFigure(drawableToAdd);
            this.setState({
                newDrawable: [],
            });
        }
    };

    handleMouseMove = e => {
        const { newDrawable } = this.state;
        if (newDrawable.length === 1) {
            const { x, y } = e.target.getStage().getPointerPosition();
            const updatedNewDrawable = newDrawable[0];
            updatedNewDrawable.registerMovement(x, y);
            this.setState({
                newDrawable: [updatedNewDrawable]
            });
        }
    };
    onPageLoadSuccess = (page) => {
        this.setState({
            page_width: page.width,
            page_height: page.height
        })
    }
    render() {
        
        const drawables = [...this.props.drawables, ...this.state.newDrawable];
        const { page_width, page_height } = this.state;
        const { pageNumber } = this.props;
        return (
            <div className="page_container" style={{
                width: page_width,
                height: page_height
            }}>
                <Page pageNumber={pageNumber}
                    onLoadSuccess={this.onPageLoadSuccess} />
                <Stage
                    className="page_convas"
                    onMouseDown={this.handleMouseDown}
                    onMouseUp={this.handleMouseUp}
                    onMouseMove={this.handleMouseMove}
                    width={page_width}
                    height={page_height}>
                    <Layer>
                        {drawables.map(drawable => {
                            return drawable.render();
                        })}
                    </Layer>
                </Stage>
            </div>
        )
    }

}
export default connect(
    (state, ownProps) => {
        debugger;
        return {
            newDrawableType: state.newDrawableType,
            drawables: state.drawables,
            ownProps
          };
    },
      dispatch => {
          return {
            onPageLoadSuccess:()=>{
                dispatch({ type: 'ON_PAGE_LOAD' });
            },
            addFigure:(figure)=>{
                dispatch({type:'ADD_FIGURE',payload:figure})
            }
          };
      }
)(DrawablePage)